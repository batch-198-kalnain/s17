/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	function personalDetail() {
		let fullName = prompt('Please enter your full name');
		console.log('Hi ' + fullName);
		let age = prompt('Please enter your age');
		console.log(fullName + '\'s age is ' + age);
		let location = prompt('Please enter your location');
		console.log(fullName + ' is located at ' + location);
	};
	personalDetail();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function favoriteBand() {
		let bandName = ['5 Seconds of Summer', 'Secondhand Serenade', 'Rocket to the Moon', 'Why don\'t we', 'Skillet'];
		console.log(bandName);
	};
	favoriteBand();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function favoriteMovies() {
		let movieTitle1 = '1. Your Name';
		let movieRating1 = 'Tomatometer for Your Name: 98%';
	
		let movieTitle2 = '2. One Piece Film Z';
		let movieRating2 = 'Tomatometer for One Piece Film Z: 96%';
		
		let movieTitle3 = '3. A silent voice';
		let movieRating3 = 'Tomatometer for A silent voice: 95%';

		let movieTitle4 = '4. Fanstastic Beast and where to find them';
		let movieRating4 = 'Tomatometer for Fanstastic Beast and where to find them: 74%';

		let movieTitle5 = '5. The curious case of Benjamin Button';
		let movieRating5 = 'Tomatometer for The curious case of Benjamin Button: 71%';	
		console.log(movieTitle1);
		console.log(movieRating1);
		console.log(movieTitle2);
		console.log(movieRating2);
		console.log(movieTitle3);
		console.log(movieRating3);
		console.log(movieTitle4);
		console.log(movieRating4);
		console.log(movieTitle5);
		console.log(movieRating5);
	};
	favoriteMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

let printFriends = function printFriendsName(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printFriends();

// console.log(friend1);
// console.log(friend2);